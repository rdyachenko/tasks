# Остановка других серверов

```
sudo supervisorctl stop app1
```

# Создание каталога с приложением
```
$ mkdir app3
$ cd app3
```

При наличии готового репозитория

```
git clone https://rdyachenko@bitbucket.org/rdyachenko/app3.git

``` 

# Создание виртуального окружения
```
$ python3 -m venv venv1
```

# Активизация виртуальной среды

Чтобы активировать новую виртуальную среду, используете следующую команду:

X-systems
```
$ source venv1/bin/activate
(venv1) $ _
```

# Установка Django

```
pip install django gunicorn
```

# Создание суперпользователя

```
python manage.py createsuperuser
Username: admin
Email address: admin@admin.com
Password:
Password (again):
Superuser created successfully.
```