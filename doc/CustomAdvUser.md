# Добавление модели расширенного пользователя

## Изменения в файлах проекта

### Файл ```tasks_core/models.py```
В файле ```models.py``` Импорт ```AbstractUser```

```
from django.contrib.auth.models import AbstractUser
```

Расширение стандартного класса ```User```

```
class AdvUser(AbstractUser):
    role_name = models.CharField(max_length=100, verbose_name="Должность", null=True,default="Должность")
    role_small_name = models.CharField(max_length=50, verbose_name="Должность (сокр)", null=True,default="Должность (сокр)")
    first_name2 = models.CharField(max_length=20, verbose_name="Отчество", null=True,default="Отчество")
    address = models.CharField(max_length=100, verbose_name="Местонахождение", null=True, default="Местонахождение")
    phone = models.CharField(max_length=100, verbose_name="Телефон", null=True, default="Телефон")
    test = models.BooleanField(verbose_name='Тестовая запись', null=True,default=False)

    def get_absolute_url(self):
        return "/advuser/%s/" % self.pk

    class Meta(AbstractUser.Meta):
        pass
```

### Файл ```tasks_core/admin.py```

Добавить импорт

```
from .models import *
```

Зарегистрировать модель

```
admin.site.register(AdvUser)
```

### Файл ```tasks/settings.py```

Добавить

```
AUTH_USER_MODEL = 'tasks_core.AdvUser'
```


## Отладка, тестирование и запуск

1. Остановить отладочный web-сервер Django. (Удалить файл базы данных).
2. Создание миграций

```manage.py makemigrations```
3. Миграция базы данных

```manage.py migrate```
4. Создание нового суперпользователя

```
manage.py createsuperuser
```

5. Запуск отладочного сервера
