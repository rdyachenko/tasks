import random
import codecs
from string import ascii_lowercase,digits
from .translit import translit
from .settings import DATAPATH, DATA


class RNDUserFromFile():
    def __init__(self):
        random.seed()
        self.load_txt("family_m_ru")
        self.load_txt("family_f_ru")
        self.load_txt("imena_m_ru")
        self.load_txt("imena_f_ru")
        self.load_txt("otch_m_ru")
        self.load_txt("otch_f_ru")
        self.load_txt("dom")
        self.load_txt("gorod")
        self.load_txt("ulica")
        self.load_txt("position")


    def load_txt(self,name):
        with codecs.open(DATAPATH / DATA[name][0], 'r', encoding='utf8') as f:
            txt = f.read()
        DATA[name].append(txt.split())


    def set_rnd_first_name(self):
        DATA["sex"] = random.randint(0, 1)
        return DATA["sex"]


    def get_rnd_first_name(self):
        if DATA["sex"]==0:
            return random.choice(DATA["imena_m_ru"][1])
        else:
            return random.choice(DATA["imena_f_ru"][1])

    def get_rnd_first_name2(self):
        if DATA["sex"]==0:
            return random.choice(DATA["otch_m_ru"][1])
        else:
            return random.choice(DATA["otch_f_ru"][1])

    def get_rnd_last_name(self):
        if DATA["sex"]==0:
            return random.choice(DATA["family_m_ru"][1])
        else:
            return random.choice(DATA["family_f_ru"][1])


    def get_rnd_email(self):
        part1 = ''.join(random.choice(ascii_lowercase) for i in range(random.randint(5,8)))
        part2 = random.choice(DATA["dom"][1])
        return "{0}@{1}".format(part1,part2)


    def get_rnd_phone(self):
        return "+"+''.join(random.choice(digits) for i in range(10))


    def get_rnd_address(self):
        p0 = ''.join(random.choice(digits) for i in range(6))
        p1 = random.choice(DATA["gorod"][1])
        p2 = random.choice(DATA["ulica"][1])
        p3 = str(random.randint(1,100))
        return "{0}, {1}, {2}, {3}".format(p0,p1,p2,p3).replace("-"," ")


    def get_rnd_full(self):
        DATA["sex"] = random.randint(0, 1)
        p0 = self.get_rnd_first_name()
        p1 = self.get_rnd_first_name2()
        p2 = self.get_rnd_last_name()
        p3 = self.get_rnd_email()
        p4 = self.get_rnd_phone()
        p5 = self.get_rnd_address()
        return "{0} {1} {2} {3} {4} {5}".format(p0,p1,p2,p3,p4,p5)


    def get_rnd_user(self):
        result = {}
        DATA["sex"] = random.randint(0, 1)
        result["first_name"]=self.get_rnd_first_name()
        result["first_name2"]=self.get_rnd_first_name2()
        result["last_name"]=self.get_rnd_last_name()
        result["email"]=self.get_rnd_email()
        result["address"]=self.get_rnd_address()
        result["phone"]=self.get_rnd_phone()
        result["position"]=self.get_rnd_position()
        result["small_position"]=self.get_rnd_small_position()
        result["username"]=translit(result["last_name"]).lower()+\
                           ''.join(random.choice(digits) for i in range(3))
        return result


    def get_rnd_position(self):
        return random.choice(DATA["position"][1]).replace("-"," ")


    def get_rnd_small_position(self):
        return "Специалист"
