from pathlib import Path

PACKAGE_NAME = "rnduser"
PACKAGE_DATA = "data"


BASE_DIR = Path(__file__).resolve().parent.parent

DOM = ["gmail.com", "yandex.ru", "hotmail.com", "mail.ru", "rambler.ru"]

DATA = {
    "family_m_ru": ["family_m_ru.txt"],
    "imena_m_ru": ["imena_m_ru.txt"],
    "otch_m_ru": ["otch_m_ru.txt"],
    "family_f_ru": ["family_f_ru.txt"],
    "imena_f_ru": ["imena_f_ru.txt"],
    "otch_f_ru": ["otch_f_ru.txt"],
    "dom": ["dom.txt"],
    "sex": 0,
    "gorod": ["gorod.txt"],
    "ulica": ["ulica.txt"],
    "position": ["position.txt"],
}

DATAPATH = BASE_DIR / PACKAGE_NAME / PACKAGE_DATA