import requests
from base64 import b64encode
import json


user='admin'
passwd='123'

# authstr = 'Basic ' + b64encode(('admin:123').encode('utf-8')).decode('utf-8')
authstr = 'Basic ' + b64encode(('{0}:{1}'.format(user,passwd)).encode('utf-8')).decode('utf-8')

# Получение списка
def test_get():
    url = 'http://127.0.0.1:8000/users/'
    r = requests.get(url, headers={'Authorization': authstr})
    data = json.loads(r.text)
    print(data)
    # for i in data['results']:
    #     print(i)

# Создание записи
def test_post():
    url = 'http://127.0.0.1:8000/users/'
    r=requests.post(url, headers={'Authorization': authstr}, data={"username":"test2","email":"3@3.ru"})
    print(r.text)

# Редактирование записи
def test_put(id,key,value):
    url="http://127.0.0.1:8000/users/{}/".format(id)
    data = {
        key: value,
    }
    r=requests.put(url, headers={'Authorization': authstr}, data=data)
    print(r.text)


# Удаление записи
def test_delete(id):
    url="http://127.0.0.1:8000/users/{}/".format(id)
    r=requests.delete(url, headers={'Authorization': authstr})
    print(r.text)


# test_put(17,"username","privet")
test_get()
# test_post()
# test_delete(16)