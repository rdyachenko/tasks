import requests
import json
from .settings import URL,USER,PASSWD
from base64 import b64encode


def get_authstr(user,passwd):
    return 'Basic ' + b64encode(('{0}:{1}'.format(user,passwd)).encode('utf-8')).decode('utf-8')


def client_api_get_list(model_name, user=USER, passwd=PASSWD):
    url = "{0}{1}/".format(URL, model_name)
    msg1 = "Sent:\nurl: {0}\nmodel name: {1}\nuser: {2}".format(url,model_name,user)
    print(msg1)
    print("\nType request: GET")
    r = requests.get(url, headers={'Authorization': get_authstr(user, passwd)})
    msg2 = "Answer: {0}".format(r.status_code)
    print(msg2)
    if r.status_code==200:
        data = json.loads(r.text)
        print(data)
    elif r.status_code==403:
        print("Error: 403")


def client_api_delete(model_name, id, user=USER, passwd=PASSWD):
    url = "{0}{1}/{2}/".format(URL, model_name,id)
    msg1 = "Sent:\nurl: {0}\nmodel name: {1}\nuser: {2}".format(url,model_name,user)
    print(msg1)
    r = requests.delete(url, headers={'Authorization': get_authstr(user, passwd)})
    msg2 = "Answer: {0}".format(r.status_code)
    print(msg2)
    if r.status_code == 204:
        data = json.loads(r.text)
        print(data)
    elif r.status_code == 403:
        print("Error: 403")
    elif r.status_code == 404:
        print("Error: 404")


def client_api_create(model_name, data={}, user=USER, passwd=PASSWD):
    url = "{0}{1}/".format(URL, model_name)
    msg1 = "Sent:\nurl: {0}\nmodel name: {1}\ndata: {2}\nuser: {3}".format(url,model_name,data,user)
    print(msg1)
    print("\nType request: GET")
    r = requests.post(url, headers={'Authorization': get_authstr(user, passwd)}, data=data)
    msg2 = "Answer: {0}".format(r.status_code)
    print(msg2)
    if r.status_code==201:
        data = json.loads(r.text)
        print(data)
    elif r.status_code == 403:
        print("Error: 403")
    elif r.status_code==400:
        print("Error: 400")


def client_api_update(model_name, id, data={}, user=USER, passwd=PASSWD):
    url = "{0}{1}/{2}/".format(URL, model_name,id)
    msg1 = "Sent:\nurl: {0}\nmodel name: {1}\nid: {2}\ndata: {3}\nuser: {4}".format(url, model_name, id, data, user)
    print(msg1)
    r = requests.put(url,
                     headers={'Authorization': get_authstr(user, passwd)},
                     data=data)
    msg2 = "Answer: {0}".format(r.status_code)
    print(msg2)
    data = json.loads(r.text)
    print(data)

