import random
import codecs
from collections import Counter
from .settings import DATAPATH

class RNDTextFromFile():
    infilename=""
    txt=""
    data=[]
    words=[]
    l1=0
    l2=0

    def __init__(self,fn=DATAPATH / "adams.txt"):
        print(fn)
        self.infilename = fn
        with codecs.open(self.infilename, 'r', encoding='utf8') as f:
            self.txt = f.read()
        self.data = self.txt.split(" ")
        self.l1 = len(self.data)
        self.words = list(set(self.data))
        self.words.sort()
        self.l2 = len(self.words)

    def gen_next_word(self,next_dict):
        s = sum(next_dict.values())
        val = random.randint(0,s)
        val_up=0
        values = list(next_dict.values())
        for i in range(s):
            val_up += values[i]
            if val <= val_up:
                word = list(next_dict.keys())[i]
                return word

    def get_dict_from_word(self,word):
        next_words = []
        for j in range(0, self.l1 - 1):
            if word == self.data[j]:
                next_word = self.data[j + 1]
                next_words.append(next_word)
        next_words = dict(Counter(next_words))
        return next_words

    def gen_rnd_sentence(self):
        s=""
        word = self.gen_next_word(self.get_dict_from_word("."))
        s = word
        while word != ".":
            d = self.get_dict_from_word(word)
            word = self.gen_next_word(d)
            s += " " + word
        s = s.capitalize()
        s = s.replace(' .', '. ')
        return s

    def gen_rnd_text(self,count):
        t = ""
        for i in range(count):
            t += self.gen_rnd_sentence()
        return t

    def get_dict_from_word2(self,word1,word2):
        next_words = []
        for j in range(0, self.l1 - 2):
            if word1 == self.data[j] and word2 == self.data[j+1]:
                next_word = self.data[j + 2]
                next_words.append(next_word)
        next_words = dict(Counter(next_words))
        return next_words

    def gen_rnd_sentence2(self):
        word1 = self.gen_next_word(self.get_dict_from_word("."))
        word2 = self.gen_next_word(self.get_dict_from_word(word1))
        s = word1 + " " + word2
        while word2 != ".":
            d = self.get_dict_from_word2(word1,word2)
            word1 = word2
            word2 = self.gen_next_word(d)
            s += " " + word2
        s = s.capitalize()
        s = s.replace(' .', '. ')
        return s

    def gen_rnd_text2(self,count):
        txt = ""
        for i in range(count):
            txt += self.gen_rnd_sentence2()
        return txt

    def gen_rnd_sentence3(self,count):
        s = ""
        word = self.gen_next_word(self.get_dict_from_word("."))
        s = word
        c=1
        while c < count:
            d = self.get_dict_from_word(word)
            word = self.gen_next_word(d)
            if word == ".":
                continue
            s += " " + word
            c += 1
        s = s.capitalize()
        s = s.replace(' .', '. ')
        return s

