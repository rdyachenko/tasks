from pathlib import Path

PACKAGE_NAME = "rndtext"
PACKAGE_DATA = "data"
BASE_DIR = Path(__file__).resolve().parent.parent
DATAPATH = BASE_DIR / PACKAGE_NAME / PACKAGE_DATA

