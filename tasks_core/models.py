from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import date


class AdvUser(AbstractUser):
    role_name = models.CharField(max_length=100, verbose_name="Должность", null=True,default="Должность")
    role_small_name = models.CharField(max_length=50, verbose_name="Должность (сокр)", null=True,default="Должность (сокр)")
    first_name2 = models.CharField(max_length=20, verbose_name="Отчество", null=True,default="Отчество")
    address = models.CharField(max_length=100, verbose_name="Местонахождение", null=True, default="Местонахождение")
    phone = models.CharField(max_length=100, verbose_name="Телефон", null=True, default="Телефон")
    test = models.BooleanField(verbose_name='Тестовая запись', null=True,default=False)

    class Meta:
        verbose_name_plural = 'Пользователи'
        verbose_name = 'Пользователь'


class Task(models.Model):
    user_owner = models.ForeignKey(AdvUser, on_delete=models.PROTECT, verbose_name="Владелец", related_name='user_owners',default=None)
    # user_executor = models.ForeignKey(AdvUser, on_delete=models.PROTECT, verbose_name="Исполнитель", related_name='user_executor',default=None)
    task_date = models.DateField(verbose_name="Дата публикации", null=True, default=date.today)
    task_title = models.CharField(max_length=500, verbose_name="Заголовок", null=True)
    task_msg = models.TextField(verbose_name="Содержание задачи", null=True)
    test = models.BooleanField(verbose_name='Тестовая запись', null=True,default=False)

    def __str__(self):
        return str(self.task_date)+" "+str(self.user_owner)+" "+str(self.task_title)

    class Meta:
        verbose_name_plural = 'Задачи'
        verbose_name = 'Задача'
        ordering = ["task_date"]


class Comment(models.Model):
    user_owner = models.ForeignKey(AdvUser, on_delete=models.PROTECT, verbose_name="Владелец", related_name='user_owners1',default=None)
    task = models.ForeignKey(Task, on_delete=models.PROTECT, verbose_name="Задача", related_name='user_owners',default=None)
    comment_date = models.DateField(verbose_name="Дата публикации", null=True, default=date.today)
    comment_msg = models.TextField(verbose_name="Комментарий к задаче", null=True)
    test = models.BooleanField(verbose_name='Тестовая запись', null=True,default=False)

    def __str__(self):
        return str(self.comment_date)+" "+str(self.user_owner)+" "+"Task:{} ".format(self.pk)+str(self.comment_msg)

    class Meta:
        verbose_name_plural = 'Комментарии'
        verbose_name = 'Комментарий'
        ordering = ["comment_date"]



