from django.core.management.base import BaseCommand
from tasks_core.models import AdvUser

class Command(BaseCommand):
    help = 'Show test user'

    def handle(self, *args, **kwargs):
        users = AdvUser.objects.filter(test=True)
        for user in users:
            print('User: '+str(user)+' deleted')
            user.delete()
