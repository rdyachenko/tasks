import random
from django.core.management.base import BaseCommand
from tasks_core.models import AdvUser,Task
from helpers.rndtext import RNDTextFromFile


class Command(BaseCommand):
    help = 'Скрипт для добавления тестовых записей в модель Task'

    arg1 = 'count'
    arg1_msg = 'Количество тестовых записей'

    def add_arguments(self, parser):
        parser.add_argument('-'+self.arg1[0], '--'+self.arg1, type=int, help=self.arg1_msg, default=1)

    def handle(self, *args, **kwargs):
        rndtxt = RNDTextFromFile.RNDTextFromFile()
        count_task = kwargs[self.arg1]
        users = AdvUser.objects.filter(test=True)
        count_user = len(users)
        for j in range(count_task):
            user_rnd_id = random.randint(0,count_user-1)

            # Создание Задачи
            task = Task()
            task.user_owner = AdvUser.objects.get(pk=users[user_rnd_id].pk)
            task.task_title = rndtxt.gen_rnd_sentence3(random.randint(6,8))
            task.task_msg = rndtxt.gen_rnd_text2(random.randint(25,30))
            task.test = True
            task.save()
            print("{0} - {1} - {2}".format(j,user_rnd_id,users[user_rnd_id].pk))
