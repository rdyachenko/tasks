from django.core.management.base import BaseCommand
from tasks_core.models import Comment


class Command(BaseCommand):
    help = 'Delete all test records of Comment'

    def handle(self, *args, **kwargs):
        comments = Comment.objects.filter(test=True)
        for comment in comments:
            print('Comment: '+str(comment)+' deleted')
            comment.delete()
