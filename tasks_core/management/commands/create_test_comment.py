import random
from django.core.management.base import BaseCommand
from tasks_core.models import AdvUser, Task, Comment
from helpers.rndtext import RNDTextFromFile


class Command(BaseCommand):
    help = 'Скрипт для добавления тестовых записей в модель Comment'

    arg1 = 'count'
    arg1_msg = 'Количество тестовых записей'

    def add_arguments(self, parser):
        parser.add_argument('-'+self.arg1[0], '--'+self.arg1, type=int, help=self.arg1_msg, default=1)

    def handle(self, *args, **kwargs):
        rndtxt = RNDTextFromFile.RNDTextFromFile()
        count_comment = kwargs[self.arg1]

        # Выявление тестовых пользователей
        users = AdvUser.objects.filter(test=True)
        count_user = len(users)

        # Выявление тестовых задач
        tasks = Task.objects.filter(test=True)
        count_task = len(tasks)

        for j in range(count_comment):
            user_rnd_id = random.randint(0,count_user-1)
            task_rnd_id = random.randint(0,count_task-1)

            # Создание Комментария
            comment = Comment()

            comment.user_owner = AdvUser.objects.get(pk=users[user_rnd_id].pk)
            comment.task = Task.objects.get(pk=tasks[task_rnd_id].pk)
            comment.comment_msg = rndtxt.gen_rnd_sentence3(random.randint(6,8))
            comment.test = True
            comment.save()
            print("{0} - {1} - {2} - {3} - {4}".format(j,
                                                       user_rnd_id,
                                                       users[user_rnd_id].pk,
                                                       task_rnd_id,
                                                       tasks[task_rnd_id].pk))
