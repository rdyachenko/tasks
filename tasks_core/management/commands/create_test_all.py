import random
from django.core.management.base import BaseCommand
from random import choice
from string import ascii_uppercase
from tasks_core.models import AdvUser,Task,Comment
from helpers.rnduser import RNDUserFromFile as rndu
from helpers.rndtext import RNDTextFromFile


class Command(BaseCommand):
    help = 'Create base all'


    def get_random_username(self):
        return 'User'+''.join(choice(ascii_uppercase) for i in range(3))


    def handle(self, *args, **kwargs):
        count_user = 3
        count_task = 10
        count_comment = 20

        test = rndu.RNDUserFromFile()
        for i in range(count_user):
            rnd_usr = test.get_rnd_user()
            user = AdvUser.objects.create_user(username=rnd_usr["username"],\
                                               password='123',\
                                               test=True,\
                                               role_name=rnd_usr["position"],\
                                               role_small_name=rnd_usr["small_position"],\
                                               first_name=rnd_usr["first_name"],\
                                               first_name2=rnd_usr["first_name2"],\
                                               last_name=rnd_usr["last_name"],\
                                               email=rnd_usr["email"],\
                                               address=rnd_usr["address"],\
                                               phone=rnd_usr["phone"])
            print("{0} Create random user name: {1}".format(i,user))
            user.save()

        rndtxt = RNDTextFromFile.RNDTextFromFile()
        users = AdvUser.objects.filter(test=True)
        count_user = len(users)
        for j in range(count_task):
            user_rnd_id = random.randint(0,count_user-1)

            # Создание Задачи
            task = Task()
            task.user_owner = AdvUser.objects.get(pk=users[user_rnd_id].pk)
            task.task_title = rndtxt.gen_rnd_sentence3(random.randint(6,8))
            task.task_msg = rndtxt.gen_rnd_text2(random.randint(25,30))
            task.test = True
            task.save()
            print("{0} - {1} - {2}".format(j,user_rnd_id,users[user_rnd_id].pk))

        # Выявление тестовых задач
        tasks = Task.objects.filter(test=True)
        count_task = len(tasks)

        for j in range(count_comment):
            user_rnd_id = random.randint(0,count_user-1)
            task_rnd_id = random.randint(0,count_task-1)

            # Создание Комментария
            comment = Comment()

            comment.user_owner = AdvUser.objects.get(pk=users[user_rnd_id].pk)
            comment.task = Task.objects.get(pk=tasks[task_rnd_id].pk)
            comment.comment_msg = rndtxt.gen_rnd_sentence3(random.randint(6,8))
            comment.test = True
            comment.save()
            print("{0} - {1} - {2} - {3} - {4}".format(j,
                                                       user_rnd_id,
                                                       users[user_rnd_id].pk,
                                                       task_rnd_id,
                                                       tasks[task_rnd_id].pk))
