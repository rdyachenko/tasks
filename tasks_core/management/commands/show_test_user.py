from django.core.management.base import BaseCommand
from random import choice
from string import ascii_uppercase
from mainapp.models import AdvUser

class Command(BaseCommand):
    help = 'Show test user'

    def handle(self, *args, **kwargs):
        users = AdvUser.objects.filter(test=True)
        for user in users:
            print(user)
