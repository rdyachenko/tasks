from django.core.management.base import BaseCommand
from tasks_core.models import Comment,AdvUser,Task


class Command(BaseCommand):
    help = 'Delete all test records'

    def handle(self, *args, **kwargs):
        comments = Comment.objects.filter(test=True)
        for comment in comments:
            print('Comment: '+str(comment)+' deleted')
            comment.delete()

        tasks = Task.objects.filter(test=True)
        for task in tasks:
            print('Task: '+str(task)+' deleted')
            task.delete()

        users = AdvUser.objects.filter(test=True)
        for user in users:
            print('User: '+str(user)+' deleted')
            user.delete()

