from django.core.management.base import BaseCommand
from tasks_core.models import Task


class Command(BaseCommand):
    help = 'Delete all test records of Task'

    def handle(self, *args, **kwargs):
        tasks = Task.objects.filter(test=True)
        for task in tasks:
            print('Task: '+str(task)+' deleted')
            task.delete()
