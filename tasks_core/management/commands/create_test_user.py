from django.core.management.base import BaseCommand
from random import choice
from string import ascii_uppercase
from tasks_core.models import AdvUser
from helpers.rnduser import RNDUserFromFile as rndu


class Command(BaseCommand):
    help = 'Create test user 2'

    def get_random_username(self):
        return 'User'+''.join(choice(ascii_uppercase) for i in range(3))

    def add_arguments(self, parser):
        parser.add_argument('-c', '--count', type=int, help='Количество тестовых пользователей',default=1)

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        test = rndu.RNDUserFromFile()
        for i in range(count):
            rnd_usr = test.get_rnd_user()
            user = AdvUser.objects.create_user(username=rnd_usr["username"],\
                                               password='123',\
                                               test=True,\
                                               role_name=rnd_usr["position"],\
                                               role_small_name=rnd_usr["small_position"],\
                                               first_name=rnd_usr["first_name"],\
                                               first_name2=rnd_usr["first_name2"],\
                                               last_name=rnd_usr["last_name"],\
                                               email=rnd_usr["email"],\
                                               address=rnd_usr["address"],\
                                               phone=rnd_usr["phone"])
            print("{0} Create random user name: {1}".format(i,user))
            user.save()
