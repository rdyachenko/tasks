from django.core.management.base import BaseCommand
from tasks_core.models import AdvUser

class Command(BaseCommand):
    help = 'Создание пользователя'

    def add_arguments(self, parser):
        parser.add_argument('-u', '--username', type=str, help='Имя пользователя')
        parser.add_argument('-p', '--password', type=str, help='Пароль')

    def handle(self, *args, **kwargs):
        name = kwargs['username']
        psd = kwargs['password']
        AdvUser.objects.create_user(username=name,password=psd)