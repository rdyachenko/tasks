from django.contrib.auth.models import Group
from tasks_core.models import AdvUser, Task, Comment
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
# class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvUser
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ['url','task_date','user_owner','task_title','task_msg']


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comment
        fields = ['url','comment_date','user_owner','task','comment_msg']

