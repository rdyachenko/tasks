# Generated by Django 3.1.5 on 2021-01-08 14:26

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tasks_core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task_date', models.DateField(default=datetime.date.today, null=True, verbose_name='Дата публикации')),
                ('task_title', models.CharField(max_length=500, null=True, verbose_name='Заголовок')),
                ('task_msg', models.TextField(null=True, verbose_name='Сообщение')),
                ('test', models.BooleanField(default=False, null=True, verbose_name='Тестовая запись')),
                ('user_owner', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Задача',
                'verbose_name_plural': 'Задачи',
                'ordering': ['task_date'],
            },
        ),
    ]
