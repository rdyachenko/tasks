# Generated by Django 3.1.5 on 2021-01-10 09:46

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tasks_core', '0006_auto_20210108_1952'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment_date', models.DateField(default=datetime.date.today, null=True, verbose_name='Дата публикации')),
                ('comment_msg', models.TextField(null=True, verbose_name='Комментарий к задаче')),
                ('test', models.BooleanField(default=False, null=True, verbose_name='Тестовая запись')),
                ('task', models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, related_name='user_owners', to='tasks_core.task', verbose_name='Задача')),
                ('user_owner', models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, related_name='user_owners1', to=settings.AUTH_USER_MODEL, verbose_name='Владелец')),
            ],
            options={
                'verbose_name': 'Комментарий',
                'verbose_name_plural': 'Комментарии',
                'ordering': ['comment_date'],
            },
        ),
    ]
