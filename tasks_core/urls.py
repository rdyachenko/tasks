from django.urls import include, path
from rest_framework import routers
from tasks_core import views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'tasks', views.TaskViewSet)
router.register(r'comments', views.CommentViewSet)


urlpatterns = [
    path('api/', include(router.urls)),
]
