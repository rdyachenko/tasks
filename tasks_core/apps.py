from django.apps import AppConfig


class TasksCoreConfig(AppConfig):
    name = 'tasks_core'
